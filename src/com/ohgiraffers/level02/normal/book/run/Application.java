package com.ohgiraffers.level02.normal.book.run;

import com.ohgiraffers.level02.normal.book.model.dto.BookDTO;

public class Application {
	public static void main(String[] args) {
		BookDTO books1 = new BookDTO();
		books1.printInformation();
		BookDTO books2 = new BookDTO("자바의 정석", "도우출판", "남궁성");
		books2.printInformation();
		BookDTO books3 = new BookDTO("홍길동전", "활빈당", "허균", 5000000, 0.5);
		books3.printInformation();
	}
}
